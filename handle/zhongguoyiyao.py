# -*- coding: utf-8 -*-
import json
import re
import jieba


class zhongGuoYiYaoHandle():
    def __init__(self):
        self.save_path = "../handle_data/中国医药信息查询平台/"
        self.data_path = "../data/中国医药信息查询平台/"
        self.department = ['胸外科', '风湿免疫科', '肿瘤科', '精神科', '传染科', '妇产科', '中医妇科', '医学检验科', '急诊科', '职业病科', '心理科', '肾内科',
                           '小儿科', '病理科', '乳腺外科', '内分泌科', '中医科', '精神心理科', '消化内科', '全科医疗科', '外科', '耳鼻喉科', '耳鼻咽喉科',
                           '康复医学科', '医疗美容科', '地方病科', '妇女保健科', '内科', '重症医学科', '特种医学与军事医学科', '儿科', '中西医结合科', '儿童保健科',
                           '眼科', '心血管内科', '妇科', '小儿外科', '预防保健科', '急诊医学科', '皮肤科', '神经外科', '神经内科', '民族医学科', '临终关怀科',
                           '医学影像科', '麻醉科', '运动医学科', '口腔科', '整形外科', '疼痛科', '结核病科', "普外科", "泌尿外科", "呼吸内科", "甲状腺外科",
                           "普通外科", "男科", "生殖医学科", "西医妇科", "感染性疾病科", "新生儿科", "肝胆外科", "康复科", "感染科", "血液内科", "中医儿科",
                           "胃肠外科", "中医内科", "遗传代谢科", "血管外科", "皮肤性病科", "胃肠外科", "肛肠外科", "骨外科", "遗传咨询科", "口腔黏膜科", "血液内科",
                           "肝病科", "脾胃病科", "中医内科", "脊柱外科", "针灸科", "性病科", "呼吸科", "血管外科", "五官科", "中医皮肤科", "变态反应科", "烧伤专科"]

    def disease(self):
        count = 0
        for element in self.department:
            jieba.add_word(element)
        f = open(self.save_path + "department.txt", 'w', encoding="utf8")
        with open(self.data_path + 'disease_content.json', encoding='utf8') as data_json:
            data_json = json.load(data_json)['content']
            for element in data_json:
                element_one = element['disease']
                if element_one['englishName']:
                    englishName = re.split('[，,，]', element_one['englishName'])
                else:
                    englishName = ''
                if element_one['otherName']:
                    otherName = re.split('[，,，、]', element_one['otherName'])
                else:
                    otherName = ''
                if element_one['inspectionItem']:
                    inspectionItem = re.split('[，,，、]', re.sub("\\(.*?\\)|\\（.*?\\）|\\（.*?\\)|\\(.*?\\）", '',
                                                               element_one['inspectionItem']))
                else:
                    inspectionItem = ''
                if element_one['relatedDrug']:
                    temp_drug = re.sub("\\(.*?\\)|\\（.*?\\）|\\（.*?\\)|\\(.*?\\）", '',
                                       element_one['relatedDrug'])
                    relatedDrug = re.split('[，,，、]',
                                           temp_drug.replace('<p>', '').replace('</p>', '').replace(
                                               '<sub>1</sub>', '').replace('&beta;-1a', '').replace('暂无相关药品。',
                                                                                                    '').replace(
                                               '<sub>12</sub>', '').replace('暂无', '').replace('<sub>6</sub>',
                                                                                              '').replace(
                                               '。', '').replace('&alpha;-', '').replace('&beta;', '').replace(
                                               '本病暂无明确特效药物。', '').replace('<sub>2</sub>', '').replace('<sub>1</sub>',
                                                                                                      '').replace(
                                               '暂无特效药。', '').replace('<sub>10</sub>', '').replace('。', '').replace(
                                               '</sub>', '').replace('&gamma;-', '').replace('<sub>2</sub>',
                                                                                             '').replace('<sub>6</sub>',
                                                                                                         '')
                                           .replace('&alpha;2b', '').replace('2<sup>', '').replace('</sup>-',
                                                                                                   '').replace(
                                               '<sub>3</sub>', '')
                                           .replace('-&alpha;-2a', '').replace('-&alpha;', '').replace('&alpha;',
                                                                                                       '').replace(
                                               '&gamma;', '').replace('<sub>3', '').replace('<sub>', '').replace(
                                               '<sub>12', '').replace('</sup>', '').replace('<sup>', '').replace('目前',
                                                                                                                 ''))
                else:
                    relatedDrug = ''
                if element_one['parts']:
                    parts = re.split('[，,，、]', element_one['parts'].replace('其他', ''))
                else:
                    parts = ''
                if element_one['mainCauses']:
                    mainCauses = element_one['mainCauses']
                else:
                    mainCauses = ''
                if element_one['multiplePopulation']:
                    multiplePopulation = '\t'.join(
                        re.split("<p>(.*?)</p>", re.sub('[\t\n\r]', '', element_one['multiplePopulation'])))
                else:
                    multiplePopulation = ''
                # print(element_one['department'], element_one['chineseName'])
                if element_one['department']:
                    department=[]
                    for children in [i for i in
                                     re.split("<p>(.*?)</p>", re.sub('[\t\n\r]', '', element_one['department'])) if
                                     i != '']:
                        for word in jieba.lcut(children):
                            if word in self.department:
                                department.append(word)
                    print(list(set(department)), element_one['chineseName'])
                    # print([i for i in re.split("<p>(.*?)</p>", re.sub('[\t\n\r]', '', element_one['department'])) if i!=''])
                    f.write(element_one['chineseName'] + '\t' + "\t".join(list(set(department)))+ '\n')
                count = count + 1
        print(count)


zhongGuoYiYaoHandle().disease()
