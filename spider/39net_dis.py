import re
import requests
from bs4 import BeautifulSoup
import json
import os
import pymongo
import uuid


class spiderCheck39():
    """
    39net检查数据爬取
    """

    def __init__(self):
        self.save_home = "../data/39net/检查/39_url.json"
        self.myclient = pymongo.MongoClient('')
        self.mydb = self.myclient['disease_knowledge']['student']
        self.headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-encoding': 'gzip, deflate',
            'Accept-language': 'zh-CN,zh;q=0.9',
            'Cache-control': 'no-cache',
            'Pragma': 'no-cache',
            'Connection': 'keep-alive',
            "Host": "jbk.39.net",
            "Upgrade-Insecure-Requests": "1",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"}

    def treat(self, url):
        result = {}
        result['内容'] = []
        result['一级标题'] = []
        result['二级标题'] = []
        response = requests.get(url, headers=self.headers)
        soup = BeautifulSoup(response.text, 'lxml')
        result['detail'] = []
        for element in soup.find_all(class_='article_text'):
            if str(element).find("article_name") == -1 and str(element).find("article_title_num") == -1 and str(
                    element).find("article_content_text") == -1:
                result['detail'].append(element.get_text())
        for element_two in soup.find_all(class_=['article_name', 'article_title_num', 'article_content_text']):
            result['内容'].append(element_two.get_text())
        for element_two in soup.find_all(class_=['article_title_num']):
            result['二级标题'].append(element_two.get_text())
        for element_two in soup.find_all(class_=['article_name']):
            result['一级标题'].append(element_two.get_text())
        result['url'] = url
        result['来源'] = '39net'
        return result

    def get_url(self):
        with open(self.save_home, encoding='utf8') as data_json:
            data_json = json.load(data_json)['url']
        result = []
        for element_ in data_json:
            print(element_['疾病名称'])
            res = {}
            res['疾病名称'] = element_['疾病名称']
            res['url'] = element_['url']
            response = requests.get(element_['url'], headers=self.headers)
            res['detail'] = []
            soup = BeautifulSoup(response.text, 'lxml')
            for element in soup.find_all(class_='left_navigation'):
                for element_one in element.find_all(class_='navigation'):
                    for element_two in element_one.find_all(class_='navigation_title navigation_cur'):
                        for element_three in element_two.find_all('a'):
                            res['detail'].append({element_three['href']: element_three.get_text()})
                    for element_four in element_one.find_all(class_='navigation_ul'):
                        for element_five in element_four.find_all('li'):
                            for element_six in element_five.find_all('a'):
                                res['detail'].append({element_six['href']: element_six.get_text()})
            result.append(res)
        with open('../data/39net/39net_url.json', "w", encoding="utf8") as dump_f:
            json.dump({"url": result}, dump_f, ensure_ascii=False, indent=2)

    def food(self, url):
        result = {}
        result['饮食建议'] = []
        # result['']
        response = requests.get(url, headers=self.headers)
        soup = BeautifulSoup(response.text, 'lxml')
        all_temp = []
        all_key = []
        for element in soup.find_all(class_='yinshi_table'):
            for element_one in element.find_all(class_='yinshi_title'):
                result['饮食建议'].append(element_one.get_text())
            food_temp = []
            temp_key = []
            for element_two in element.find_all('tbody'):
                temp_value = []
                for element_three in element_two.find_all("tr"):
                    value_ = []
                    key_ = []
                    for element_five in element_three.find_all("th"):
                        key_.append(element_five.get_text())
                    for element_four in element_three.find_all("td"):
                        value_.append(element_four.get_text())
                    if value_ != []:
                        temp_value.append(value_)
                    if key_ != []:
                        temp_key.append(key_)
                all_key.append(temp_key)
                food_temp.append(temp_value)
            all_temp.append(food_temp)
        print(result['饮食建议'])
        print(all_temp)
        print(all_key)

    def get_all(self):
        f = open("error1.txt", 'w', encoding="utf8")
        with open('../data/39net/39net_url.json', encoding='utf8') as data_json:
            data_json = json.load(data_json)['url']
        for element_one in data_json:
            print(element_one['疾病名称'])
            for element in element_one['detail']:
                for key, value in element.items():
                    if value == '病因' or value == '并发症' or value == '症状' or value == '预防' or value == '鉴别' or value == '治疗' or value == '检查' or value == '护理':
                        try:
                            data = self.treat(key)
                            with open('../data/39net/疾病/' + element_one['疾病名称'] + '_' + value + '.json', "w",
                                      encoding="utf8") as dump_f:
                                json.dump(data, dump_f, ensure_ascii=False, indent=2)
                        except:
                            print('../data/39net/疾病/' + element_one['疾病名称'] + '_' + value + '.json' + "fail")
                            f.write('../data/39net/疾病/' + element_one['疾病名称'] + '_' + value + '.json' + '\n')

    def handle(self, file):
        result = []
        with open('../data/39net/疾病/' + file, encoding='utf8') as data_json:
            data_json = json.load(data_json)
            if data_json['detail'] != []:
                result = re.split("\n", ''.join(data_json['detail']))
            result.extend(data_json['内容'])
        return '<br>'.join(result)

    def to_mongo(self):
        with open('../data/39net/39net_url.json', encoding='utf8') as data_json:
            data_json = json.load(data_json)['url']
        data_file = [file for file in os.listdir('../data/39net/疾病/')]
        result =[]
        for element_one in data_json:
            temp ={}
            temp['jbmc']  =element_one['疾病名称']
            print(temp['jbmc'])
            if element_one['疾病名称'] + '_' + "病因" + '.json' in data_file:
                temp['by'] = self.handle(element_one['疾病名称'] + '_' + "病因" + '.json')
            if element_one['疾病名称'] + '_' + "并发症" + '.json'in data_file:
                temp['bfz'] = self.handle(element_one['疾病名称'] + '_' + "并发症" + '.json')
            if element_one['疾病名称'] + '_' + "症状" + '.json'in data_file:
                temp['lcbx'] = self.handle(element_one['疾病名称'] + '_' + "症状" + '.json')
            if element_one['疾病名称'] + '_' + "预防" + '.json' in data_file:
                temp['yf'] = self.handle(element_one['疾病名称'] + '_' + "预防" + '.json')
            if element_one['疾病名称'] + '_' + "鉴别" + '.json' in data_file:
                temp['jbzd'] = self.handle(element_one['疾病名称'] + '_' + "鉴别" + '.json')
            if element_one['疾病名称'] + '_' + "治疗" + '.json' in data_file:
                temp['zl'] = self.handle(element_one['疾病名称'] + '_' + "治疗" + '.json')
            if element_one['疾病名称'] + '_' + "检查" + '.json' in data_file:
                temp['sysjc'] = self.handle(element_one['疾病名称'] + '_' + "检查" + '.json')
            if element_one['疾病名称'] + '_' + "护理" + '.json' in data_file:
                temp['yh'] = self.handle(element_one['疾病名称'] + '_' + "护理" + '.json')
            result.append(temp)
            res = self.mydb.insert_one(temp)

        with open('9net_url.json', "w", encoding="utf8") as dump_f:
            json.dump({"url": result}, dump_f, ensure_ascii=False, indent=2)


print(spiderCheck39().to_mongo())
# print(spiderCheck39().get_url())
# print(spiderCheck39().treat("http://jbk.39.net/xselny/jcjb/"))
# print(spiderCheck39().food('http://jbk.39.net/fhgm/ysbj/'))
# 0 1 0 1
# 1 0 1 1
# 1 1 1 1
# 0 1 0 1
# 1 0 1 0
# 0 1 1 0
#


# 1 2 4
# 0 2 3 5
# 1 2 4 5
# 1 2 3 4

# 0 3 5
# 1 4
# 0 3
# 0 5
# 035140305
# 0 1 3 4 5
# 022130305

